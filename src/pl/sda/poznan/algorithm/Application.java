package pl.sda.poznan.algorithm;


import java.util.Scanner;

public class Application {

    public static int NWD(int a, int b) {

        while (a != b) {
            if (a < b) {
                b = b - a;
            } else {
                a = a - b;
            }
        }
        return a;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwie liczby, dla ktorych znalezc NWD");
        int result = NWD(scanner.nextInt(), scanner.nextInt());
        System.out.println("NWD = " + result);
    }
}
