package pl.sda.poznan.recursion;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        printMenu();
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                System.out.println("Podaj n");
                System.out.println(MathHelper.factorial(scanner.nextInt()));
                break;
            case 2:
                System.out.println("Podaj n");
                System.out.println(MathHelper.fibonacci(scanner.nextInt()));
                break;
            default:
                System.out.println("Nieprawidlowy wybor");

        }
    }

    public static void printMenu() {
        System.out.println("Witaj, co chcesz zrobic?");
        System.out.println("1. Obliczyc silnie n!");
        System.out.println("2. Obliczyc n-ty wyraz ciagu Fibonacciego");
    }
}
