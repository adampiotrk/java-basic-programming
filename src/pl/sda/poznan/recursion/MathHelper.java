package pl.sda.poznan.recursion;

public class MathHelper {
    private MathHelper() {
    }


    /**
     * Obliczenia silnii rekurencyjnie
     */
    public static int factorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("n must be greater than 0");
        }
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    /**
     * Obliczenie n-tego wyrazu ciagu Fibonacciego rekurencyjnie
     */
    public static int fibonacci(int n) {
        int result;
        if (n < 0) {
            throw new IllegalArgumentException("n must be greater than 0");
        }
        if (n == 0) {
            result = 0;
        } else if (n == 1 || n == 2) {
            result = 1;
        } else {
            result = fibonacci(n - 1) + fibonacci(n - 2);
        }

        return result;
    }
}
