package pl.sda.poznan.sort;

public class QuickSort {
    private int[] numbers;

    public void sort(int[] values) {
        //sprawdzenie czy tablica nie jest pusta
        if (values == null || values.length == 0) {
            return;
        }
        this.numbers = values;
        quicksort(0, numbers.length - 1);
    }

    private void quicksort(int low, int high) {
        int i = low; // -> poczatek lewej listy
        int j = high; // -> koniec prawej listy
        // wybierz srodkowy element
        int midIndex = (low + high) / 2;
        int pivot = numbers[midIndex];
        // przejdz lewa czesc i prawa czesc listy
        // wykonuj tak dlugo az indeksy lewej i prawy sie zejda
        while (i <= j) {
            while (numbers[i] < pivot) {
                i++;
            }
            while (numbers[j] > pivot) {
                j--;
            }
            if (i <= j) {
                // zamien miejscami
                int helper = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = helper;
                i++;
                j--;
            }
        }
        if (low < j) {
            quicksort(low, j);
        }
        if (i < high) {
            quicksort(i, high);
        }
    }
}
