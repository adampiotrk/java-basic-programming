package pl.sda.poznan.sort;

import org.junit.Test;

import static org.junit.Assert.*;

public class BubbleSortTest {
    @Test
    public void shouldSortArray() {
        int[] arrayOfInts = {9, 5, 7, 12, 3};
        BubbleSort.sort(arrayOfInts);
        System.out.println();
    }

    @Test
    public void shouldSortStrings() {
        String[] arrayOfStrings = {"Zenek", "alan", "Marek", "Beata", "Adam", "Natalia", "Alan", "BEATA"};
        BubbleSort.sortString(arrayOfStrings);
        System.out.println();
    }

}