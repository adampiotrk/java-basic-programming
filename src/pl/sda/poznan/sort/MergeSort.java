package pl.sda.poznan.sort;

public class MergeSort {
    private int[] numbers;
    private int[] helper;

    public void sort(int[] values) {
        this.numbers = values;
        this.helper = new int[numbers.length];
        mergeSort(0, numbers.length - 1);
    }

    private void mergeSort(int low, int high) {
        // jezeli indeks startowy < indeksu koncowego to tablica ma wiecej niz 1 element czyli mozna ja podzielic
        //jezeli tablica ma 1 element to low i high = 0 - nie da sie juz podzielic tablicy
        if (low < high) {
            //oblicz srodkowy indeks
            int middle = low + (high - low) / 2;
            // posortuj lewa czesc tablicy
            mergeSort(low, middle);
            // posortuj prawa czesc tablicy
            mergeSort(middle + 1, high);
            //scalaj
            merge(low, middle, high);
        }
    }

    // algorytm scalania dwoch tablic
    private void merge(int low, int middle, int high) {
        //przekopiowanie do pomocniczej
        for (int i = low; i <= high; i++) {
            helper[i] = numbers[i];
        }
        //poczatek lewej tablic
        int i = low; // poczatek lewej tablicy
        int j = middle + 1; // poczatek prawej tablicy
        // do wpisywania do scalonej tablicy, zaczynamy od lewej
        int k = low;
        //tak dlugo az dojdziemy do konca jednej z tablic
        while (i <= middle && j <= high) {
            //element z lewej strony jest mniejszy
            if (helper[i] < helper[j]) {
                numbers[k] = helper[i];
                i++;
            } else { // element z prawej strony jest mniejszy
                numbers[k] = helper[j];
                j++;
            }
            k++;
        }
        // skopiuj reszte lewej tablicy
        while (i <= middle) {
            numbers[k] = helper[i];
            i++;
            k++;
        }
    }
}
