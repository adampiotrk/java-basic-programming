package pl.sda.poznan.sort;

/**
 * na poczatku zakladamy, ze z lewej strony mamy zbior 1-elementowy, ktory juz jest posortowany -> i=1
 * wybieramy pierwszy element ze zbioru nieposortowanego - zmienna j
 * probujemy wstawic do zbioru posortowanego
 * jezeli napotkamy mniejszy element to zamieniamy elementy
 */
public class InsertionSort {
    public static void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int j = i; j > 0; j--) {
                //przechodzimy przez zbior posortowany od tylu
                if (array[j] < array[j - 1]) {
                    int helper = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = helper;
                }
            }
        }
    }
}
