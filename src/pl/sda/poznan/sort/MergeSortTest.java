package pl.sda.poznan.sort;

import org.junit.Test;

import static org.junit.Assert.*;

public class MergeSortTest {
    @Test
    public void sort() {
        int[] numbers = {9, 5, 2, 3, 6, 1};
        int[] sortedNumers = {1, 2, 3, 5, 6, 9};
        MergeSort mergeSort = new MergeSort();
        mergeSort.sort(numbers);
        assertArrayEquals(sortedNumers, numbers);
    }

}