package pl.sda.poznan.sort.measure;

import pl.sda.poznan.sort.*;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Program wypelniajacy tablice losowymi wartosciami
 * Mierzy czas sortowania roznymi metodami i wyswietla czas sortowania
 */
public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Podaj ilosc elementow, ktore wylosowac");
        int size = scanner.nextInt();
        int[] randomArray = new int[size];
        for (int i = 0; i < randomArray.length; i++) {
            randomArray[i] = random.nextInt(100000000) + 1;
        }

        int[] toSort = Arrays.copyOf(randomArray, size);
        //sortowanie babelkowe
        System.out.println("Sortowanie babelkowe");
        System.out.println("Rozpoczynam sortowanie...");
        long startTime = System.currentTimeMillis();
        BubbleSort.sort(toSort);
        long endTime = System.currentTimeMillis();
        long time = endTime - startTime;
        System.out.println("Sortowanie zakonczone. Czas: " + time + " ms");
        System.out.println();

        //sortowanie mergeSort
        System.out.println("Sortowanie przez scalanie");
        System.out.println("Rozpoczynam sortowanie...");
        int[] toSortByMerge = Arrays.copyOf(randomArray, size);
        MergeSort mergeSort = new MergeSort();
        long startTimeMerge = System.currentTimeMillis();
        mergeSort.sort(toSortByMerge);
        long endTimeMerge = System.currentTimeMillis();
        long timeMerge = endTimeMerge - startTimeMerge;
        System.out.println("Sortowanie zakonczone. Czas: " + timeMerge + " ms");
        System.out.println();

        //sortowanie bucket
        System.out.println("Sortowanie kubelkowe");
        System.out.println("Rozpoczynam sortowanie...");
        int[] toSortByBucket = Arrays.copyOf(randomArray, size);
        long startTimeBucket = System.currentTimeMillis();
        BucketSort.sort(toSortByBucket);
        long endTimeBucket = System.currentTimeMillis();
        long timeBucket = endTimeBucket - startTimeBucket;
        System.out.println("Sortowanie zakonczone. Czas: " + timeBucket + " ms");
        System.out.println();

        //sortowanie przez wstawianie
        System.out.println("Sortowanie przez wstawianie");
        System.out.println("Rozpoczynam sortowanie...");
        int[] toSortByInsert = Arrays.copyOf(randomArray, size);
        long startTimeInsert = System.currentTimeMillis();
        InsertionSort.sort(toSortByInsert);
        long endTimeInsert = System.currentTimeMillis();
        long timeInsert = endTimeInsert - startTimeInsert;
        System.out.println("Sortowanie zakonczone. Czas: " + timeInsert + " ms");
        System.out.println();

        //sortowanie QuickSort
        System.out.println("Sortowanie QuickSort");
        System.out.println("Rozpoczynam sortowanie...");
        int[] toQuickSort = Arrays.copyOf(randomArray, size);
        QuickSort quickSort = new QuickSort();
        long startTimeQuick = System.currentTimeMillis();
        quickSort.sort(toQuickSort);
        long endTimeQuick = System.currentTimeMillis();
        long timeQuick = endTimeQuick - startTimeQuick;
        System.out.println("Sortowanie zakonczone. Czas: " + timeQuick + " ms");
        System.out.println();
    }


}
