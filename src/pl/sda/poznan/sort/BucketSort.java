package pl.sda.poznan.sort;

// zakladamy, ze mamy liczby z przedzialu <0;max>
public class BucketSort {

    public static void sort(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        sort(arr, max);
    }

    private static void sort(int[] arr, int max) {
        //tworzymy tablice na kubelki
        int[] bucket = new int[max + 1];
        for (int i = 0; i < arr.length; i++) {
            bucket[arr[i]]++;
        }

        //zewnetrzna petla przechodzi przez tablice koszykow
        int k = 0;
        for (int i = 0; i < bucket.length; i++) {
            // wewnetrzna petla wykonuje sie tyle razy ile bylo elementow w koszyku
            for (int j = 0; j < bucket[i]; j++) {
                arr[k] = i;
                k++;
            }
        }

    }
}
