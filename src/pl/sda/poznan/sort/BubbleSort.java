package pl.sda.poznan.sort;

// zlozonosc czasowa O(n^2)
// zlozonosc pamieciowa O(1)
public class BubbleSort {
    public static void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length - i; j++) {
                if (array[j] < array[j - 1]) {
                    int helper = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = helper;
                }
            }
        }
    }

    public static void sortString(String[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length - i; j++) {
                if (array[j].compareToIgnoreCase(array[j - 1]) < 0) {
                    String helper = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = helper;
                }
            }
        }
    }
}
