package pl.sda.poznan.sort;

import org.junit.Test;

import static org.junit.Assert.*;

public class BucketSortTest {
    @Test
    public void sort() {
        int[] numbers = {9, 5, 1, 2, 3, 9, 5, 3, 6, 1, 11, 10, 17, 15};
        int[] sortedNumbers = {1, 1, 2, 3, 3, 5, 5, 6, 9, 9, 10, 11, 15, 17};
        BucketSort.sort(numbers);
        assertArrayEquals(sortedNumbers, numbers);
    }

}