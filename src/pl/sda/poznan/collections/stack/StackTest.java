package pl.sda.poznan.collections.stack;

import org.junit.Test;

import static org.junit.Assert.*;

public class StackTest {
    Stack<String> myStack = new Stack<>();

    @Test
    public void shouldPushNewElements() {
        assertEquals(0, myStack.size());
        myStack.push("Jeden");
        assertEquals(1, myStack.size());
        myStack.push("Dwa");
        assertEquals(2, myStack.size());
    }

    @Test
    public void shouldPopElement() {
        assertEquals(null, myStack.pop());
        myStack.push("Jeden");
        myStack.push("Dwa");
        myStack.push("Trzy");
        assertEquals(3, myStack.size());
        assertEquals("Trzy", myStack.pop());
        assertEquals(2, myStack.size());
        assertEquals("Dwa", myStack.pop());
        assertEquals(1, myStack.size());
        assertEquals("Jeden", myStack.pop());
        assertEquals(0, myStack.size());
        assertEquals(true, myStack.isEmpty());
    }

    @Test
    public void shouldClearStack() {
        myStack.push("Jeden");
        myStack.push("Dwa");
        myStack.push("Trzy");
        myStack.clear();
        assertEquals(true, myStack.isEmpty());
        assertEquals(0, myStack.size());
    }

    @Test
    public void shouldFindElement() {
        myStack.push("Jeden");
        myStack.push("Dwa");
        myStack.push("Trzy");
        assertEquals(0, myStack.search("Trzy"));
        assertEquals(1, myStack.search("Dwa"));
        assertEquals(2, myStack.search("Jeden"));
        assertEquals(-1, myStack.search("BRAK"));

    }
}