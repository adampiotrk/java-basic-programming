package pl.sda.poznan.collections.stack;

/**
 * Struktura danych, gdzie dodajemy dane na wierzcholek stosu i usuwamy/zdejmujemy elementy z wierzcholka
 * Typ LIFO - last in last out
 *
 * @param <E>
 */
public class Stack<E> {
    private Node<E> top;
    private int size;

    public Stack() {
        this.size = 0;
    }

    public int size() {
        return size;
    }

    // odkladanie elementu na stos, zawsze odkladamy na szczyt stosu
    public void push(E element) {
        Node<E> newNode = new Node<E>(element);
        newNode.setPrev(top);
        top = newNode;
        size++;
    }

    // pobieranie elementu za stosu, zawsze pobieramy szczytowy element
    public E pop() {
        if (isEmpty()) {
            return null;
        }
        E elementToPop = top.getData();
        top = top.getPrev();
        size--;
        return elementToPop;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void clear() {
        top = null;
        size = 0;
    }

    // zwraca element top bez usuwania
    public E peek() {
        return isEmpty() ? null : top.getData();

//        ponizej robi to samo co zapis powyzej, wyzej forma skrocona
//        if (isEmpty()) {
//            return null;
//        }
//        return top.getData();
    }


    // metoda ma zwracac czy element wystepuje na stosie
    // jezeli element jest na szczycie to ma zwracac 1
    // jezeli tak to ile elementow od elementu top
    // jezeli nie to zwracamy -1
    public int search(E element) {
        int index = 1;
        Node<E> helper = top;
        while (helper != null) {
            if (helper.getData().equals(element)) {
                return index;
            }
            helper = helper.getPrev();
            index++;
        }
        return -1;
    }
}
