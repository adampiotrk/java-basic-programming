package pl.sda.poznan.collections.linkedlist;

import pl.sda.poznan.collections.generic.GenericList;

/**
 * Implementacja listy jednokierukowej z dowiazaniami
 * w tej implementacji mamy wskazanie na glowe listy
 * Ostatni element listy ma wskazanie next ustawione na null - po tym poznajemy koniec listy
 *
 * @param <E>
 */
public class SingleLinkedList<E> implements GenericList<E> {

    private Node<E> head;
    private int size;

    public SingleLinkedList() {
        this.size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public boolean add(E element) {
        size++;
        // 1 przypadek - lista jest pusta
        if (head == null) {
            head = new Node<E>(element);
        } else {
            insertDataAtBeginning(element);
        }
        // 2 przypadek - lista juz ma elementy
        return false;
    }

    private void insertDataAtBeginning(E element) {
        // O(1)
        Node<E> newNode = new Node<E>(element);
        newNode.setNext(head);
        head = newNode;
    }

    private void insertDataAtTheEnd(E element) {
        // zmienna pomocnicza do iterowania po liscie - na poczatku ustawiamy ja na glowe listy
        // helper i head wskazuja na glowe listy
        // O(n), gdzie n to rozmiar listy
        Node<E> helper = head;
        while (helper.getNext() != null) {
            helper = helper.getNext();
        }
        Node<E> newNode = new Node<E>(element);
        helper.setNext(newNode);
    }

    @Override
    public void add(int index, E element) {
        Node<E> current = getNodeByIndex(index);
        if (current == head) {
            size++;
            insertDataAtBeginning(element);
            return;
        }
        Node<E> prev = getNodeByIndex(index - 1);
        Node<E> newNode = new Node<E>(element);
        prev.setNext(newNode);
        newNode.setNext(current);
        size++;

    }

    /**
     * Prywatna metoda do zwracania wezla o indeksie
     *
     * @param index
     * @return
     */
    private Node<E> getNodeByIndex(int index) {
        Node<E> helper = head;
        int i = 0;
        while (i != index) {
            helper = helper.getNext();
            i++;
        }
        return helper;
    }

    /**
     * W tej implementacji kazdy element ma wskazanie tylko na nastepny element
     * wiec potrzebujemy elementu do usuniecia i elementu poprzedzajacego
     * Helper ustawiamy na glowe, a prev na null, poniewaz poprzednik glowy ma wartosc null
     *
     * @param element
     * @return
     */
    @Override
    public boolean remove(E element) {
        if (!contains(element)) {
            return false;
        }

        Node<E> helper = head;
        Node<E> prev = null;
        while (helper.getNext() != null) {
            if (helper.getValue().equals(element)) {
                break;
            }
            prev = helper;
            helper = helper.getNext();
        }
        // jezeli poprzednik wynosi null to oznacza to, ze mamy skasowac pierwszy element - glowe
        // wiec referencje head przestawiamy o jeden element dalej
        // else w poprzedniku usuwanego elementu ustawiamy jego nastepnik na wartosc nastepnika aktualnie usuwanego elementu
        if (prev == null) {
            head = helper.getNext();
        } else {
            prev.setNext(helper.getNext());
        }
        // wstaw null do referencji GC usunie ten obiekt z pamieci
        helper = null;
        size--;
        return true;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;

    }

    @Override
    public E get(int index) {
        //sprawdzenie czy operujemy na dobrym zakresie
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Wrong index");
        }
        // sprawdzenie czy lista jest pusta
        if (head == null) {
            return null;
        }

        Node<E> helper = head;
        int step = 0;
        while (step != index) {
            helper = helper.getNext();
            step++;
        }
        return helper.getValue();
    }

    @Override
    public E remove(int index) {
        E element = this.get(index);
        this.remove(element);
        return element;
    }

    @Override
    public E remove() {
        return remove(size - 1);

//        Node<E> helper = head;
//        Node<E> prev = null;
//        while (helper.getNext() != null) {
//            prev = helper;
//            helper = helper.getNext();
//        }
//        size--;
//        prev.setNext(null);
//        return helper.getValue();
    }

    @Override
    public int indexOf(E element) {
        int index = 0;

        Node<E> helper = head;
        while (helper != null) {
            if (helper.getValue().equals(element)) {
                return index;
            }
            helper = helper.getNext();
            index++;
        }
        return -1;
    }


    @Override
    public void print() {
        Node<E> helper = head;
        while (helper.getNext() != null) {
            System.out.println(helper.getValue().toString());
            helper = helper.getNext();
        }
        System.out.println(helper.getValue().toString());
    }
}
