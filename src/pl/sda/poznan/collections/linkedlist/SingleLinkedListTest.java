package pl.sda.poznan.collections.linkedlist;

import org.junit.Test;

import static org.junit.Assert.*;

public class SingleLinkedListTest {

    @Test
    public void shouldAddAndPrintNewValues() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");

        myList.print();
        assertEquals(4, myList.size());


    }

    @Test
    public void shouldGetByIndex() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");

        assertEquals("Bartek", myList.get(2));


    }

    @Test
    public void shouldGetByIndexWhenEmpty() {
        SingleLinkedList<String> myList = new SingleLinkedList();

        assertEquals(null, myList.get(0));

    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldGetByIndexWhenWrongIndex() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.get(-4);

    }

    @Test
    public void shouldReturnIndexByElement() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");

        assertEquals(0, myList.indexOf("Marzena"));
        assertEquals(1, myList.indexOf("Kasia"));
        assertEquals(2, myList.indexOf("Bartek"));
        assertEquals(3, myList.indexOf("Adam"));
    }

    @Test
    public void shouldReturnIndexMinusOneWhenElementNotFound() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");

        assertEquals(-1, myList.indexOf("Marek"));
    }

    @Test
    public void shouldRemoveElement() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");

        myList.remove("Bartek");
        assertEquals(3, myList.size());
        assertEquals(-1, myList.indexOf("Bartek"));

    }

    @Test
    public void shouldRemoveHeadByElement() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");

        myList.remove("Marzena");
        assertEquals(3, myList.size());
        assertEquals(-1, myList.indexOf("Marzena"));
    }

    @Test
    public void shouldRemoveLastElement() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");

        myList.remove("Adam");
        assertEquals(3, myList.size());
        assertEquals(-1, myList.indexOf("Adam"));
    }

    @Test
    public void shouldReturnFalseWhenRemovingElementNotFound() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");

        assertEquals(false, myList.remove("Grzechu"));

    }

    @Test
    public void shouldRemoveFirstByIndex() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");
        assertEquals("Marzena", myList.remove(0));
    }

    @Test
    public void shouldRemoveByIndex() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");
        assertEquals("Bartek", myList.remove(2));
    }

    @Test
    public void shouldClearList() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");
        myList.clear();
        assertEquals(0, myList.size());
    }

    @Test
    public void shouldDeleteLastElement() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");
        assertEquals("Adam", myList.remove());
        assertEquals(3, myList.size());
        assertEquals("Bartek", myList.remove());
        assertEquals(2, myList.size());
        assertEquals("Kasia", myList.remove());
        assertEquals(1, myList.size());
        assertEquals("Marzena", myList.remove());
        assertEquals(0, myList.size());
    }

    @Test
    public void shouldAddByElementAtBeginning() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");
        myList.add(0, "Krzychu");
        assertEquals(0, myList.indexOf("Krzychu"));
        assertEquals(1, myList.indexOf("Marzena"));
        assertEquals(5, myList.size());
    }

    @Test
    public void shouldAddElementByIndex() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");
        myList.add(2, "Krzychu");
        assertEquals(2, myList.indexOf("Krzychu"));
        assertEquals(3, myList.indexOf("Bartek"));
        assertEquals(1, myList.indexOf("Kasia"));
        assertEquals(5, myList.size());
    }

    @Test
    public void shouldAddLastElementByIndex() {
        SingleLinkedList<String> myList = new SingleLinkedList();
        myList.add("Adam");
        myList.add("Bartek");
        myList.add("Kasia");
        myList.add("Marzena");
        myList.add(4, "Krzychu");
        assertEquals(4, myList.indexOf("Krzychu"));
        assertEquals(3, myList.indexOf("Adam"));
        assertEquals(5, myList.size());
    }
}