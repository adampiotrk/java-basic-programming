package pl.sda.poznan.collections.doublelinkedlist;

import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleLinkedListTest {
    private DoubleLinkedList<String> myList = new DoubleLinkedList<>();

    @Test
    public void addNewElementToEmptyList() {
        myList.add("Adam");
        assertEquals(0, myList.indexOf("Adam"));
        assertEquals(1, myList.size());
    }

    @Test
    public void addNewElementsToList() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals(0, myList.indexOf("Adam"));
        assertEquals(1, myList.indexOf("Beata"));
        assertEquals(2, myList.indexOf("Marek"));
        assertEquals(3, myList.indexOf("Kasia"));
        assertEquals(4, myList.indexOf("Jan"));
        assertEquals(5, myList.size());
    }

    @Test
    public void getElementByIndex() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals("Adam", myList.get(0));
        assertEquals("Beata", myList.get(1));
        assertEquals("Marek", myList.get(2));
        assertEquals("Kasia", myList.get(3));
        assertEquals("Jan", myList.get(4));
    }

    @Test
    public void removeLastElement() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals("Jan", myList.remove());
        assertEquals(4, myList.size());
        assertEquals("Kasia", myList.remove());
        assertEquals(3, myList.size());
        assertEquals("Marek", myList.remove());
        assertEquals(2, myList.size());

    }

    @Test
    public void removeFirstElement() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals("Adam", myList.removeFromBeginning());
        assertEquals(4, myList.size());
        assertEquals("Beata", myList.removeFromBeginning());
        assertEquals(3, myList.size());
        assertEquals("Marek", myList.removeFromBeginning());
        assertEquals(2, myList.size());
    }

    @Test
    public void shouldRemoveByElement() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals(true, myList.remove("Beata"));
        assertEquals(0, myList.indexOf("Adam"));
        assertEquals(1, myList.indexOf("Marek"));
        assertEquals(false, myList.contains("Beata"));
        assertEquals(4, myList.size());
    }

    @Test
    public void shouldRemoveFirstByElement() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals(true, myList.remove("Adam"));
        assertEquals(0, myList.indexOf("Beata"));
        assertEquals(1, myList.indexOf("Marek"));
        assertEquals(false, myList.contains("Adam"));
        assertEquals(4, myList.size());
    }

    @Test
    public void shouldRemoveLastByElement() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals(true, myList.remove("Jan"));
        assertEquals(0, myList.indexOf("Adam"));
        assertEquals(1, myList.indexOf("Beata"));
        assertEquals(false, myList.contains("Jan"));
        assertEquals(4, myList.size());
    }

    @Test
    public void shouldReturnFalseWhenRemovingElementDoNotExist() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals(false, myList.remove("Kakaka"));
        assertEquals(5, myList.size());
    }

    @Test
    public void shouldAddElementByIndex() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        myList.add(0, "Basia");
        myList.add(2, "Natalia");
        myList.add(7, "Bartek");
        assertEquals("Basia", myList.get(0));
        assertEquals("Natalia", myList.get(2));
        assertEquals("Bartek", myList.get(7));
        assertEquals(8, myList.size());
    }

    @Test
    public void shouldRemoveElementByIndex() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals("Marek", myList.remove(2));
        assertEquals(false, myList.contains("Marek"));
        assertEquals(4, myList.size());
    }

    @Test
    public void shouldRemoveElementByIndexAtBeginning() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals("Adam", myList.remove(0));
        assertEquals(false, myList.contains("Adam"));
        assertEquals(4, myList.size());
    }

    @Test
    public void shouldRemoveElementByIndexAtEnd() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals("Jan", myList.remove(4));
        assertEquals(false, myList.contains("Jan"));
        assertEquals(4, myList.size());
    }

    @Test
    public void shouldClearTheList() {
        myList.add("Adam");
        myList.add("Beata");
        myList.add("Marek");
        myList.add("Kasia");
        myList.add("Jan");
        assertEquals(5, myList.size());
        myList.clear();
        assertEquals(true, myList.isEmpty());
        assertEquals(0, myList.size());
    }
}