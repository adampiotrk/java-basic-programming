package pl.sda.poznan.collections.doublelinkedlist;

import pl.sda.poznan.collections.generic.GenericList;

public class DoubleLinkedList<E> implements GenericList<E> {
    private Node<E> first;
    private Node<E> last;
    private int size;

    public DoubleLinkedList() {
        this.size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public boolean add(E element) {
        size++;
        if (first == null) {
            first = new Node<E>(element);
            last = first;
        } else {
            Node<E> newNode = new Node<E>(element);
            last.setNext(newNode);
            newNode.setPrev(last);
            last = newNode;
        }
        return true;
    }

    @Override
    public void add(int index, E element) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Wrong index");
        }
        if (index == 0) {
            Node<E> nodeToAdd = new Node<E>(element);
            first.setPrev(nodeToAdd);
            nodeToAdd.setNext(first);
            first = nodeToAdd;
            size++;
        } else if (index == size) {
            add(element);
        } else {
            Node<E> nodeToAdd = new Node<E>(element);
            Node<E> prevNode = getNodeByIndex(index - 1);
            Node<E> currentNode = getNodeByIndex(index);
            prevNode.setNext(nodeToAdd);
            currentNode.setPrev(nodeToAdd);
            nodeToAdd.setPrev(prevNode);
            nodeToAdd.setNext(currentNode);
            size++;
        }

    }

    @Override
    public boolean remove(E element) {
        if (!contains(element)) {
            return false;
        }
        if (getNodeByElement(element) == first) {
            removeFromBeginning();
        } else if (getNodeByElement(element) == last) {
            remove();
        } else {

            Node<E> prevNode = getNodeByElement(element).getPrev();
            Node<E> nextNode = getNodeByElement(element).getNext();
            prevNode.setNext(nextNode);
            nextNode.setPrev(prevNode);
            size--;
        }
        return true;
    }

    private Node<E> getNodeByElement(E element) {
        if (!contains(element)) {
            return null;
        }

        Node<E> helper = first;
        while (!helper.getData().equals(element)) {
            helper = helper.getNext();
        }
        return helper;
    }


    private Node<E> getNodeByIndex(int index) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Wrong index");
        }
        if (first == null) {
            return null;
        }
        Node<E> helper = first;
        get(index);
        int i = 0;
        while (i != index) {
            helper = helper.getNext();
            i++;
        }
        return helper;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;

    }

    @Override
    public E get(int index) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Wrong index");
        }
        if (first == null) {
            return null;
        }

        Node<E> helper;
        if (index <= (size - 1) / 2) {
            int step = 0;
            helper = first;
            while (step != index) {
                helper = helper.getNext();
                step++;
            }
        } else {
            helper = last;
            int step = size - 1;
            while (step != index) {
                helper = helper.getPrev();
                step--;
            }
        }
        return helper.getData();
    }

    @Override
    public E remove(int index) {
        E elementToDelete = get(index);
        remove(elementToDelete);
        return elementToDelete;
    }

    @Override
    public E remove() {
        E lastElement = last.getData();
        last = last.getPrev();
        last.setNext(null);
        size--;
        return lastElement;
    }

    public E removeFromBeginning() {
        E firstElement = first.getData();
        first = first.getNext();
        first.setPrev(null);
        size--;
        return firstElement;
    }

    @Override
    public int indexOf(E element) {
        int index = 0;
        Node<E> helper = first;
        while (helper != null) {
            if (helper.getData().equals(element)) {
                return index;
            }
            helper = helper.getNext();
            index++;
        }
        return -1;
    }

    @Override
    public void print() {
        Node<E> helper = first;
        while (helper.getNext() != null) {
            System.out.println(helper.getData());
            helper = helper.getNext();
        }
        System.out.println(helper.getData());
    }
}
