package pl.sda.poznan.collections.generic;

import pl.sda.poznan.collections.Collection;

public interface GenericList<E> extends Collection<E> {

    void add(int index, E element);

    boolean remove(E element);

    E get(int index);

    E remove(int index);

    void print();
}


