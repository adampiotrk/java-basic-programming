package pl.sda.poznan.collections.generic;

import java.util.Arrays;

public class GenericArrayList<E> implements GenericList<E> {

    private Object[] values;
    private int size;
    private static final int INITIAL_SIZE = 20;

    public GenericArrayList() {
        this.size = 0;
        values = new Object[INITIAL_SIZE];
    }

    public GenericArrayList(int size) {
        this.size = 0;
        this.values = new Object[size];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public boolean add(E element) {
        add(size + 1, element);
        return false;
    }

    @Override
    public void add(int index, E element) {
        checkRange(index);
        if (size == values.length) {
            values = Arrays.copyOf(values, size * 2);
        }

        Object[] restOfTheList = Arrays.copyOfRange(values, index - 1, size);
        size++;

        int j = 0;
        for (int i = index; i < size; i++) {
            values[i] = restOfTheList[j++];
        }
        values[index - 1] = element;
    }

    @Override
    public boolean remove(E element) {
        int i = indexOf(element);
        Object deleted = remove(i + 1);
        return deleted != null;
    }

    @Override
    public void clear() {
        size = 0;
        int newSize = values.length;
        values = new Object[newSize];

    }

    @Override
    public E get(int index) {
        checkRange(index);
        return (E) values[index - 1];
    }


    @Override
    public E remove(int index) {
        checkRange(index);
        Object toDelete = values[index - 1];
        values[index - 1] = null;

        Object[] restOfList = Arrays.copyOfRange(values, index, size);
        size--;

        int j = 0;
        for (int i = index - 1; i < size; i++) {
            values[i] = restOfList[j++];
        }
        values[size] = null;
        return (E) toDelete;
    }

    @Override
    public E remove() {
        int indexOfLastElement = size - 1;
        Object toDelete = values[indexOfLastElement];
        values[indexOfLastElement] = null;
        size--;
        return (E) toDelete;
    }

    @Override
    public int indexOf(E element) {
        for (int i = 0; i < size; i++) {
            if (values[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.print(values[i] + ", ");
        }
        System.out.println();
    }

    private void checkRange(int index) {
        if (index - 1 < 0 || index - 1 > size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }
}
