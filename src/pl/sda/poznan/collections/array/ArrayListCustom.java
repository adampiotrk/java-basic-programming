package pl.sda.poznan.collections.array;

import java.util.Arrays;

/**
 * Lista wykorzystujaca tablice (implementacja podobna do klasy java.util.ArrayList
 * Lista przechowuje wartosci typu String
 * Lista powinna miec nastepujace funkcje
 * - zwracanie rozmiaru listy
 * - odpowiedz na pytanie czy lista jest pusta
 * - dodawanie elementu na koniec listy
 * - dodawanie w okreslonej pozycji listy
 * - usuwanie po okreslonej wartosci
 * - wyczyszczenie wszystkich wartosci
 * - pobieranie po indeksie
 * - usuwanie po indeksie
 */
public class ArrayListCustom implements MyList {
    // w tej tablicy bedziemy przechowywac wartosci tablicy
    private String[] values;
    // zmienna okreslajaca rozmiar tablicy, w przypadku gdy uzytkownik z gory wie jaka duza ma byc tablica
    private int size;
    // stala okreslajaca wielkosc tablicy w przypadku tworzenia standardowej tablicy, w przypadku przekroczenia tej wartosci tablica trzeba skopiowac do wiekszej
    private static final int INITIAL_SIZE = 20;

    // konstruktor bez parametrowy - domyslna wielkosc tablicy
    public ArrayListCustom() {
        this.size = 0;
        this.values = new String[INITIAL_SIZE];
    }

    // tablica przyjmujaca rozmiar, gdy wiadomo ile elementowa tablice utworzyc
    public ArrayListCustom(int size) {
        this.size = 0;
        this.values = new String[size];
    }

    @Override
    public int size() {
        return this.size;
        // - w ten sposob zwrocimy rzeczywisty rozmiar tablicy
//        return values.length; - ta opcja zwroci wartosc 20, mimo ze tablica jest pusta
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
//         analogiczny zapis z ifem
//        if (size == 0) {
//            return true;
//        } else {
//            return false;
//        }
    }

    @Override
    public boolean contains(String o) {
        return indexOf(o) >= 0;

        // 1. napisanie petli for - ktora sprawdza wszystkie elementy i zwraca true, jezeli szukany element wystepuje
        // 2. zauwazenie, ze to samo robi metoda indexOf(s) - mozemy z niej korzystac bez powtarzania petli for
    }

    @Override
    public boolean add(String e) {
        // wywolujemy funkcje add na ostatnim miejscu, zeby nie powtarzac metody
        add(size + 1, e);
        return false;
    }

    @Override
    public void add(int index, String element) {
        checkRange(index);
        // przekopiuj stara tablice do nowej 2* wiekszej
        // jezeli mamy pelna tablice
        if (size == values.length) {
            values = Arrays.copyOf(values, size * 2);
        }

        String[] restOfTheList = Arrays.copyOfRange(values, index - 1, size);
        size++;

        int j = 0;
        for (int i = index; i < size; i++) {
            values[i] = restOfTheList[j++];
        }
        values[index - 1] = element;


    }

    @Override
    public boolean remove(String o) {
        int i = indexOf(o);
        String deleted = remove(i + 1);
        return deleted != null;
    }

    @Override
    public void clear() {
        // zerujemy rozmiar i tworzymy nowa pusta tablice, stala zlozonsc, niezalezna od rozmiaru
        // stara tablica zostanie usunieta przez Garbage Collectro (GC)
        size = 0;
        int newSize = values.length;
        // przypisanie null powoduje utracenie referencji do oryginalnej tablicy
        // values = null; - opcjonalne, to samo robi funkcja ponizej
        values = new String[newSize];

//        metoda z petla for ma zlozonosc n, czym wiekszy rozmiar, tym wieksza zlozonosc (n)
//        for (int i = 0; i < size; i++) {
//            values[i] = null;
//        }
//        this.size = 0;
    }

    @Override
    public String get(int index) {
        checkRange(index);
        return values[index - 1];
    }

    @Override
    public String remove(int index) {
        checkRange(index);
        String toDelete = values[index - 1];
        values[index - 1] = null;
        String[] restOfList = Arrays.copyOfRange(values, index, size);
        size--;

        int j = 0;
        for (int i = index - 1; i < size; i++) {
            values[i] = restOfList[j++];
        }
        values[size] = null;
        return toDelete;
    }

    @Override
    public String removeLastElement() {
        int indexOfLastElement = size - 1;
        String toDelete = values[indexOfLastElement];
        values[indexOfLastElement] = null;
        size--;
        return toDelete;
    }

    /**
     * Metoda zwracajaca pozycje elementu na liscie
     * Jezeli wystepuje zwracamy jego indeks
     * Jezeli nie wystepuje zwracamy -1
     * Na pewno indeks w tablicy nie bedzie mniejszy od zera wiec zwracamy -1
     * * @param s - szukany element
     * * @return
     */
    @Override
    public int indexOf(String s) {
        for (int i = 0; i < size; i++) {
            if (values[i].equals(s)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.print(values[i] + ", ");
        }
        System.out.println();
    }

    private void checkRange(int index) {
        if (index - 1 < 0 || index - 1 > size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }
}
