package pl.sda.poznan.collections.array;

/**
 * Interfejs dla nie generycznej Array Listy
 */
public interface MyList {
    /**
     * Zwraca aktualny rozmiar listy
     *
     * @return
     */
    int size();

    /**
     * Odpowiada na pytanie czy lista jest pusta
     *
     * @return
     */
    boolean isEmpty();

    /**
     * Zwraca true, jezeli element znajduje sie na liscie
     *
     * @param o
     * @return
     */
    boolean contains(String o);

    /**
     * Dodaje okreslony napis na koniec listy
     *
     * @param e
     * @return
     */
    boolean add(String e);

    /**
     * Dodaje element na okreslona pozycje
     *
     * @param index
     * @param element
     */
    void add(int index, String element);

    /**
     * Usuwa element podany jako argument (o ile taki wystepuje)
     */
    boolean remove(String o);

    /**
     * Czysci liste z wszystkich elementow
     */
    void clear();

    /**
     * Zwraca element po indeksie (po pozycji)
     *
     * @param index
     * @return
     */
    String get(int index);

    /**
     * Usuwa element na okreslonej pozycji i zwraca usuniety element
     *
     * @param index
     * @return
     */
    String remove(int index);

    /**
     * Usuwa ostatni element z listy
     * @return
     */
    String removeLastElement();

    void print();

    /**
     * Zwraca indeks (pozycje) pierwszego wystapienia okreslonego wpisu
     *lub -1 jezeli wartosc nie wystepuje
     * @param s
     * @return
     */
    int indexOf(String s);
}
