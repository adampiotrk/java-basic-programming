package pl.sda.poznan.collections.array;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArrayListCustomTest {

    @Test
    public void shouldRemoveLastElement() {
        MyList stringList = new ArrayListCustom();
        stringList.add("Maciek");
        stringList.add("Zbyszek");
        stringList.add("Zuzanna");
        String deleted = stringList.removeLastElement();
        // powyzej kod testu
        // ponizej sprawdzamy czy test jest zakonczony
        // oczekiwanymi wartosciami
        assertEquals(2, stringList.size());
        assertEquals("Zuzanna", deleted);
    }

    // w tym tescie oczekujemy wyjatku
    // test zakonczy sie powodzeniem, jesli taki wyjatek zostanie rzucony
    // podajemy nazwe wyjatku jako argument adnotacji
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void shouldReturnErrorWithEmptyList() {
        MyList stringList = new ArrayListCustom();
        stringList.removeLastElement();
    }

    @Test
    public void shouldContainElement() {
        MyList list = new ArrayListCustom();
        list.add("Adam");
        boolean isElementPresent = list.contains("Adam");
        assertEquals(true, isElementPresent);
    }

    @Test
    public void shouldNotContainElement() {
        MyList list = new ArrayListCustom();
        list.add("Jacek");
        boolean isElementPresent = list.contains("Margaret");
        assertEquals(false, isElementPresent);
    }

    @Test
    public void shouldDeleteByIndex() {
        MyList list = new ArrayListCustom();
        list.add("Ala");
        list.add("ma");
        list.add("kota");
        list.add("Gosia");
        list.add("chce");
        list.add("psa");
        String removed = list.remove(3);

        assertEquals("kota", removed);
        assertEquals(5, list.size());
    }

    @Test
    public void shouldDeleteByElement() {
        MyList list = new ArrayListCustom();
        list.add("Ala");
        list.add("ma");
        list.add("kota");
        list.add("Gosia");
        list.add("chce");
        list.add("psa");
        boolean isDeleted = list.remove("Gosia");

        assertEquals(true, isDeleted);
        assertEquals(5, list.size());
    }

    @Test
    public void shouldAddElementAtIndex() {
        MyList list = new ArrayListCustom();
        list.add("Ala");
        list.add("ma");
        list.add("Gosia");
        list.add("chce");
        list.add("psa");
        list.add(3, "kota");

        assertEquals("kota", list.get(3));
        assertEquals(6, list.size());
    }


}