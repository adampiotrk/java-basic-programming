package pl.sda.poznan.collections;

import pl.sda.poznan.collections.array.ArrayListCustom;
import pl.sda.poznan.collections.array.MyList;

public class Program {
    public static void main(String[] args) {
        MyList stringList = new ArrayListCustom(5);
        stringList.add("Maciej");
        stringList.add("Zbyszek");
        stringList.add("Grzesiek");
        stringList.add("Czesiek");
        stringList.add("Bozena");


        stringList.print();
        try {
            System.out.println(stringList.get(5));
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Nie ma elementu o takim indeksie!");
        }

        stringList.add(5,"Grzechu");
        System.out.println(stringList.get(5));
        System.out.println(stringList.get(6));

        // 1. Przetestowac dodawanie
        // 2. Napisac metode clear - kasujace wszystkie lementy
        // 3. Napisac metode get(int index)
        // 4. Napisac metode remove (int index)
    }
}
