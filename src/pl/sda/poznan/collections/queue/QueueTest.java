package pl.sda.poznan.collections.queue;

import org.junit.Test;

import static org.junit.Assert.*;

public class QueueTest {
    private Queue<String> myQueue = new Queue<>();

    @Test
    public void returnsTrueIfEmpty() {
        assertEquals(true, myQueue.isEmpty());
    }

    @Test
    public void shouldAddElements() {
        assertEquals(0, myQueue.size());
        assertEquals(true, myQueue.isEmpty());
        myQueue.add("Marek");
        assertEquals(1, myQueue.size());
        myQueue.add("Staszek");
        assertEquals(2, myQueue.size());
        myQueue.add("Anna");
        assertEquals(3, myQueue.size());
        myQueue.add("Kasia");
        assertEquals(4, myQueue.size());
        assertEquals(false, myQueue.isEmpty());

    }

    @Test
    public void shouldRemoveElements() {
        myQueue.add("Marek");
        myQueue.add("Staszek");
        myQueue.add("Anna");
        myQueue.add("Kasia");
        assertEquals("Marek", myQueue.remove());
        assertEquals(3, myQueue.size());
        assertEquals("Staszek", myQueue.remove());
        assertEquals(2, myQueue.size());
        assertEquals("Anna", myQueue.remove());
        assertEquals(1, myQueue.size());
        assertEquals("Kasia", myQueue.remove());
        assertEquals(0, myQueue.size());
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void shouldThrowExceptionWhenRemoveFromEmptyQueue() {
        myQueue.add("Marek");
        assertEquals("Marek", myQueue.remove());
        myQueue.remove();
    }

    @Test
    public void shouldClearTheQueue() {
        myQueue.add("Marek");
        myQueue.add("Staszek");
        myQueue.add("Anna");
        myQueue.add("Kasia");
        myQueue.clear();
        assertEquals(0, myQueue.size());
        assertEquals(true, myQueue.isEmpty());
    }

    @Test
    public void shouldReturnBooleanIfElementInTheQueue() {
        myQueue.add("Marek");
        myQueue.add("Staszek");
        myQueue.add("Anna");
        myQueue.add("Kasia");
        assertEquals(true, myQueue.contains("Marek"));
        assertEquals(true, myQueue.contains("Staszek"));
        assertEquals(true, myQueue.contains("Anna"));
        assertEquals(true, myQueue.contains("Kasia"));
        assertEquals(false, myQueue.contains("BRAK"));
    }

    @Test
    public void shouldReturnIndexOfElement() {
        myQueue.add("Marek");
        myQueue.add("Staszek");
        myQueue.add("Anna");
        myQueue.add("Kasia");
        assertEquals(0, myQueue.indexOf("Marek"));
        assertEquals(1, myQueue.indexOf("Staszek"));
        assertEquals(2, myQueue.indexOf("Anna"));
        assertEquals(3, myQueue.indexOf("Kasia"));
        assertEquals(-1, myQueue.indexOf("BRAK"));
    }


}