package pl.sda.poznan.collections.queue;

import pl.sda.poznan.collections.Collection;

/**
 * Kolejka - struktura danych gdzie dodajemy na koncu listy, a usuwamy na poczatku
 * Typ FIFO - first in, first out
 *
 * @param <E>
 */
public class Queue<E> implements Collection<E> {
    //kolejka pozwala dodawac tylko na koncu i usuwa z przodu
    private Node<E> first;
    private Node<E> last;
    private int size;

    public Queue() {
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public boolean add(E element) {
        Node<E> newNode = new Node<>(element);
        if (isEmpty()) {
            first = newNode;
            last = first;
        } else {
            last.setNext(newNode);
            newNode.setPrev(last);
            last = newNode;
        }
        size++;
        return true;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;

    }

    @Override
    public E remove() {
        if (isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Queue is empty");
        }

        E elementToRemove = first.getData();
        if (size == 1) {
            first = null;
            last = null;
        } else {
            first = first.getNext();
            first.setPrev(null);
        }
        size--;
        return elementToRemove;
    }

    @Override
    public int indexOf(E element) {
        int index = 0;
        Node<E> helper = first;
        while (helper != null) {
            if (helper.getData().equals(element)) {
                return index;
            }
            helper = helper.getNext();
            index++;
        }
        return -1;
    }
}
