package pl.sda.poznan.collections;

public interface Collection<E> {
    int size();

    boolean isEmpty();

    boolean contains(E element);

    boolean add(E element);

    void clear();

    E remove();

    int indexOf(E element);

}
