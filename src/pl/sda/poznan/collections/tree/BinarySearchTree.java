package pl.sda.poznan.collections.tree;

public class BinarySearchTree<T extends Comparable<T>> implements Tree<T> {

    private Node<T> root;

    @Override
    public void insert(T data) {
        if (root == null) {
            this.root = new Node<>(data);
        } else {
            insert(data, root);
        }

    }

    private void insert(T data, Node<T> node) {
        // porownujemy wstawiany element z danym wezlem
        // jezeli bezie wartosc ujemna to znaczy, ze element musi trafic do lewego poddrzewa
        if (data.compareTo(node.getData()) < 0) {
            // wstawiamy do lewego poddrzewa
            if (node.getLeft() != null) {
                // jezeli ma syna, musimy rekurencyjnie wykonac jeszcze raz insert
                insert(data, node.getLeft());
            } else {
                Node<T> newNode = new Node<>(data);
                node.setLeft(newNode);
            }
        } else {
            if (node.getRight() != null) {
                insert(data, node.getRight());
            } else {
                Node<T> newNode = new Node<>(data);
                node.setRight(newNode);
            }
        }
    }

    @Override
    public void delete(T data) {
        if (root != null) {
            root = delete(root, data);
        }
    }

    private Node<T> delete(Node<T> node, T data) {
        if (node == null) {
            return null;
        }
        // szukamy element do usuniecia w lewym poddrzewie
        if (data.compareTo(node.getData()) < 0) {
            node.setLeft(delete(node.getLeft(), data));
            // szukamy element do usuniecia w prawym poddrzewie
        } else if (data.compareTo(node.getData()) > 0) {
            node.setRight(delete(node.getRight(), data));
            // znalezlismy element do usuniecia
        } else {
            // usuwanie liscia
            if (node.getLeft() == null && node.getRight() == null) {
                System.out.println("Removing leaf ... ");
                return null;
            }

            // usuwanie rodzica z 1 dzieckiem
            if (node.getLeft() == null) {
                System.out.println("Removing right ... ");
                Node<T> tempNode = node.getRight();
                node = null;
                return tempNode;
            }

            if (node.getRight() == null) {
                System.out.println("Removing left ... ");
                Node<T> tempNode = node.getLeft();
                node = null;
                return tempNode;
            } else {
                // usuwanie rodzica z 2 dzieci
                System.out.println("Removing with 2 items ...");
                Node<T> tempNode = getPredecessor(node.getLeft());
                node.setData(tempNode.getData());
                node.setLeft(delete(node.getLeft(), tempNode.getData()));
            }
        }

        return node;
    }

    private Node<T> getPredecessor(Node<T> node) {
        if (node.getRight() != null) {
            return getPredecessor(node.getRight());
        }
        return node;
    }


    @Override
    public T getMax() {
        if (root == null) {
            return null;
        }
        if (root.getRight() == null) {
            return root.getData();
        } else {
            return getMax(root.getRight());
        }
    }

    private T getMax(Node<T> node) {
        if (node.getRight() != null) {
            return getMax(node.getRight());
        }
        return node.getData();
    }

    @Override
    public T getMin() {
        if (root == null) {
            return null;
        }
        if (root.getLeft() == null) {
            return root.getData();
        } else {
            return getMin(root.getLeft());
        }
    }

    private T getMin(Node<T> node) {
        if (node.getLeft() != null) {
            return getMin(node.getLeft());
        }
        return node.getData();
    }

    @Override
    public void traversal() {
        if (root != null) {
            System.out.println("--- INORDER ---");
            inOrderTraversal(root);
            System.out.println("--- PREORDER ---");
            preOrderTraversal(root);
            System.out.println("--- POSTORDER ---");
            postOrderTraversal(root);
        }

    }

    /**
     * Przejscie InOrder przez drzewo, LVR - lewy syn, korzen, prawy syn
     *
     * @param node
     */
    private void inOrderTraversal(Node<T> node) {
        // odwiedz lewego syna
        if (node.getLeft() != null) {
            inOrderTraversal(node.getLeft());
        }
        // odwiedz rodzica
        System.out.println(node.getData().toString());
        // odwiedz prawego syna
        if (node.getRight() != null) {
            inOrderTraversal(node.getRight());
        }
    }

    /**
     * Przejscie PreOrder - VLR
     *
     * @param node
     */
    private void preOrderTraversal(Node<T> node) {
        System.out.println(node.getData().toString());
        if (node.getLeft() != null) {
            preOrderTraversal(node.getLeft());
        }
        if (node.getRight() != null) {
            preOrderTraversal(node.getRight());
        }
    }

    /**
     * Przejscie PostOrder - LRV
     *
     * @param node
     */
    private void postOrderTraversal(Node<T> node) {
        if (node.getLeft() != null) {
            postOrderTraversal(node.getLeft());
        }
        if (node.getRight() != null) {
            postOrderTraversal(node.getRight());
        }
        System.out.println(node.getData().toString());
    }
}
