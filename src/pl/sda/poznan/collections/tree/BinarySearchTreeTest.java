package pl.sda.poznan.collections.tree;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class BinarySearchTreeTest {
    Tree<Integer> myTree = new BinarySearchTree();

    @Test
    public void shouldInsertElements() {
        myTree.insert(15);
        myTree.insert(20);
        myTree.insert(10);
        myTree.insert(5);
        myTree.insert(13);
        myTree.insert(18);
        myTree.insert(23);
        myTree.traversal();

    }

    @Test
    public void shouldReturnMax() {
        myTree.insert(15);
        myTree.insert(20);
        myTree.insert(10);
        myTree.insert(5);
        myTree.insert(13);
        myTree.insert(18);
        myTree.insert(23);

        assertEquals(23, (int) myTree.getMax());
    }

    @Test
    public void shouldReturnMin() {
        myTree.insert(15);
        myTree.insert(20);
        myTree.insert(10);
        myTree.insert(5);
        myTree.insert(13);
        myTree.insert(18);
        myTree.insert(23);

        assertEquals(5, (int) myTree.getMin());
    }

    @Test
    public void shouldRemoveLeaf() {
        myTree.insert(15);
        myTree.insert(20);
        myTree.insert(10);
        myTree.insert(5);
        myTree.insert(13);
        myTree.insert(18);
        myTree.insert(23);

        assertEquals(5, (int) myTree.getMin());
        myTree.delete(5);
        assertEquals(10, (int) myTree.getMin());
        myTree.traversal();
    }

    @Test
    public void shouldRemoveParentWithOneChild() {
        myTree.insert(15);
        myTree.insert(10);
        myTree.insert(20);
        myTree.insert(13);
        myTree.insert(18);
        myTree.insert(23);

        myTree.traversal();
        myTree.delete(10);
        myTree.traversal();
    }

    @Test
    public void shouldRemoveParentWithTwoChildrens() {
        myTree.insert(15);
        myTree.insert(10);
        myTree.insert(20);
        myTree.insert(13);
        myTree.insert(18);
        myTree.insert(23);
        myTree.insert(5);

        myTree.traversal();
        myTree.delete(10);
        myTree.traversal();
    }

    @Test
    public void shouldRemoveRoot() {
        myTree.insert(15);
        myTree.insert(10);
        myTree.insert(20);
        myTree.insert(13);
        myTree.insert(18);
        myTree.insert(23);

        myTree.traversal();
        myTree.delete(15);
        myTree.traversal();
    }


}