package pl.sda.poznan.collections.tree;

public interface Tree<T> {
    /**
     * Metoda dodawania elementu do drzewa
     *
     * @param data
     */
    void insert(T data);

    /**
     * Metoda usuwania elementu drzewa
     *
     * @param data
     */
    void delete(T data);

    /**
     * Metoda znajdujaca najwiekszy element
     */
    T getMax();

    /**
     * Metoda znajdujaca najmniejszy element
     */
    T getMin();

    /**
     * Przejscie przez drzewo
     */
    void traversal();
}
