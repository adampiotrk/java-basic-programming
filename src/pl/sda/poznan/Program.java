package pl.sda.poznan;

public class Program {
    public static void main(String[] args) {
        Person person1 = new Person("Adam", 25);
        Person person2 = new Person("Marek", 30);
        Person person3 = new Person("Aga", 18);
        Person person4 = new Person("Bartek", 25);

        System.out.println(person1.compareTo(person2));
        System.out.println(person1.compareTo(person3));
        System.out.println(person1.compareTo(person4));

    }
}
